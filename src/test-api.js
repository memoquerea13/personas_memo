import { LitElement, html } from 'lit-element';  
	class TestApi extends LitElement {
static get properties() {
	return {			
		title: {type: String},
        movies: {type: Array}
	};
}

constructor() {
	super();		

	this.title = " ";		
    this.movies = [];
    this.getMovieData();
}

render() {
	return html`	
		<!-- Enlace Bootstrap -->		
		<div>
			<h3> ${this.title} </h3>
            <ul>
                ${this.movies.map(
                    function(movie)
                    {
                        return html`<l> la pelicula ${movie.title}, fue dirigida por ${movie.director} </li> <br>`;
                        
                    }
                )}
            </ul>
		</div>			
	`;
}       	

getMovieData()
{
    console.log("getMovieData");
    console.log("obteniendo datos de las peliculas");

    let xhr= new XMLHttpRequest();

    xhr.onload = function(){
        if (xhr.status===200) {
            console.log("petición correctamente");
            let APIRespose = JSON.parse(xhr.responseText);
            this.movies = APIRespose.results;
            console.log(this.movies);
        }
    
    }.bind(this);
xhr.open("GET","https://swapi.dev/api/films/");
xhr.send();
}
}
customElements.define('test-api', TestApi)